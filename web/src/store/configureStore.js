import {createStore, compose} from 'redux';
import rootReducer from '../reducers'

//Enhancer to allow Redux debugging via chrome redux extension tool
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

//Create the Redux store/state
const configureStore = (initialState) => {
    return createStore(
        rootReducer,/* preloadedState, */
        initialState,
        composeEnhancers(),
    )
};

export default configureStore;
