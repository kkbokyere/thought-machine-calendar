import types from '../constants/action-types'

export const addEvent = event => ({
    type: types.ADD_EVENT,
    payload: event
});
