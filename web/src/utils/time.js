export const createTimeIntervals = (from, until, interval = 30) => {
    //"01/01/2001" is just an arbitrary date
    until = Date.parse("01/01/2001 " + until);
    from = Date.parse("01/01/2001 " + from);
    //*2 because we want every 30 minutes instead of every hour
    const max = (Math.abs(until-from) / (60*60*1000))*(60 / interval);
    const time = new Date(from);
    const hours = [];
    for(let i = 0; i <= max; i++) {
        //doubleZeros just adds a zero in front of the value if it's smaller than 10.
        const hour = time.getHours();
        const minute = time.getMinutes();
        hours.push(hour + ":" + minute);
        time.setMinutes(time.getMinutes() + interval);
    }
    return hours;
};
