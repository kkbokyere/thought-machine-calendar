import {createTimeIntervals} from '../../utils/time';

test('should return time intervals', () => {
    const times = createTimeIntervals("09:00","10:00");
    expect(times).toEqual(['9:0','9:30','10:0']);
});
