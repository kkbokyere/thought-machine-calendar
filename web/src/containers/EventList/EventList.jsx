import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import EventItem from '../../components/EventItem';
import styles from './EventList.module.scss'

const EventList = ({ events }) => {
  return (
      <div className={styles['event-list']}>
          {events.map((times, index) => (<EventItem key={index} {...times}/>))}
      </div>
  )
};

EventList.defaultProps = {
    events: []
};

EventList.propTypes = {
    events: PropTypes.array
};

const mapStateToProps = ({ events }) => ({
    events
});
export default connect(
    mapStateToProps,
    null
)(EventList)

