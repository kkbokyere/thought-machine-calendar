import React from 'react'
import PropTypes from 'prop-types'
import styles from './Timeline.module.scss'
import {createTimeIntervals} from "../../utils/time";
import {DEFAULT_HALF_HOUR_HEIGHT} from "../../constants";

const Timeline = () => {
  const times = createTimeIntervals("09:00","18:00");
  return <div className={styles.timeline}>
    {times.map((time) =>
        <div key={time}
             style={{
               height: DEFAULT_HALF_HOUR_HEIGHT
             }}
             className={styles['time-block']}
        >
          <span>{time}</span>
        </div>)}
  </div>
};

Timeline.defaultProps = {
  start: 0,
  end: 0
};

Timeline.propTypes = {
  start: PropTypes.number,
  end: PropTypes.number
};

export default Timeline;
