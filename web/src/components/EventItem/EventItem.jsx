import React from 'react'
import PropTypes from 'prop-types'
import styles from './EventItem.module.scss'
import {DEFAULT_HALF_HOUR_HEIGHT} from '../../constants';

const EventItem = ({ start, end }) => {
  const height = ((end - start) / DEFAULT_HALF_HOUR_HEIGHT) * DEFAULT_HALF_HOUR_HEIGHT;
  const top = (start / DEFAULT_HALF_HOUR_HEIGHT) * DEFAULT_HALF_HOUR_HEIGHT;
  return (
      <div style={{
        top,
        height
      }} className={styles['event-item']}>{start} {end}</div>
  )
};

EventItem.defaultProps = {
  start: 0,
  end: 0
};

EventItem.propTypes = {
  start: PropTypes.number,
  end: PropTypes.number
};

export default EventItem;
