import types from '../constants/action-types'

export const initialState = [{start: 0, end: 0}];

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case types.ADD_EVENT:
            return [
                ...state,
                payload
            ];
        default:
            return state
    }
}
