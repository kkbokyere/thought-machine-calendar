import React from 'react';
import './styles/App.scss'
import './utils/window'
import EventList from './containers/EventList';
import Timeline from "./components/Timeline";
const events = [{start: 30, end: 60}, {start: 90, end: 120}];

function App() {
  return (
    <div className="App">
      <EventList data={events}/>
      <Timeline/>
    </div>
  );
}

export default App;
